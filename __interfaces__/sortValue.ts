export type sortValue = {
    sortByKey: string,
    sortMethod: string,
};