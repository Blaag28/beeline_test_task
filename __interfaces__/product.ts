export type Product = {
    id : string,
    date: number,
    name: string,
    count: string,
    status: boolean,
    priority: string
};