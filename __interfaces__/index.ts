export * from './inputsModal';
export * from './product';
export * from './sortValue';
export * from './changeValueProduct';
export * from './newProduct';

export type Nullable<T> = T | null;

/* Расширяем интерфейс window для Redux DevTools */
declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
        dataLayer: any;
    }
}
