export type ModalInputs = {
    name: string,
    count: string,
    priority: string
};
