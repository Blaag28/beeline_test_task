export type NewProduct = {
    name: string;
    count: string;
    priority: string;
};