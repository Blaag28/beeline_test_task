export type ChangeValue = {
    itemId: string;
    value: boolean;
};