export const getTimestamp = () => new Date().getTime() / 1000;
export const getLocaleTimes = (value) => new Date(value * 1000).toLocaleString();
