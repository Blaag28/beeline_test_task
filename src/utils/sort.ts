import {SORT_METHOD} from '@constants/index';

export const sortArr = (valueOne, valueTwo, type = SORT_METHOD.DESC) => {

    const result = type === SORT_METHOD.ASC ? {value1: -1, value2: 1} : {value1: 1, value2: -1}

    if (valueOne > valueTwo) {
        return result.value1
    }
    if (valueOne < valueTwo) {
        return result.value2
    }
    return 0
};