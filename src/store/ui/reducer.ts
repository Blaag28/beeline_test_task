import { createReducer } from 'redux-act';

import {SORT_BY_KEY, SORT_METHOD} from '@constants/index';

import * as applicationActions from './actions';

const initialState = {
    sortByKey: SORT_BY_KEY.DATE,
    sortMethod : SORT_METHOD.ASC
};

const reducer = createReducer({}, initialState);

reducer.on(applicationActions.changeSort, (state, value) => ({
    ...state,
    ...value
}));

export default reducer;
