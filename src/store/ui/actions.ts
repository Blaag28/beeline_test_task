import { createAction } from 'redux-act';

import { SORT_METHOD } from '@constants/index';

export const moduleName = 'ui';

export const changeSort = createAction<any>(`${moduleName}/CHANGE_SORT_APPLICATION`);

export const editSort = (sortByKey, sortMethod) => (dispatch) => {

    const value = {
        sortByKey: sortByKey,
        sortMethod : sortMethod === SORT_METHOD.ASC ? SORT_METHOD.DESC : SORT_METHOD.ASC
    };

    return dispatch(changeSort(value))
};
