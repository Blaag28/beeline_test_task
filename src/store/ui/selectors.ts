import { createSelector } from 'reselect';

import { moduleName } from './actions';

const storeSelector = (store) => store[moduleName];

export const getSort = createSelector(storeSelector, (store) => store);
