import { combineReducers } from 'redux';

import applicationReducer from '@store/application';
import uiReducer from '@store/ui';

export default combineReducers({
    application: applicationReducer,
    ui: uiReducer
});
