import { createAction } from 'redux-act';
import { ChangeValue, NewProduct } from '@interfaces/index';
import { v4 as uuidv4 } from 'uuid';

import { getProduct } from '@store/application/selectors';
import { getTimestamp } from '@utils/times';

export const moduleName = 'application';

export const addItem = createAction<any>(`${moduleName}/ADD_ITEM_APPLICATION`);
export const addProduct = createAction<any>(`${moduleName}/ADD_PRODUCT_APPLICATION`);

export const addProducts = (data: NewProduct) => (dispatch) => {
    dispatch(
        addItem({
            ...data,
            id: uuidv4(),
            status: false,
            date: getTimestamp()
        })
    );
};

export const changeStatusProducts = ({ itemId, value }: ChangeValue) => (dispatch, getState) => {
    const allProducts = getProduct(getState());

    //TODO можно по рефачить, но время 3 ночи))
    const changeProducts = allProducts.map((data) => {
        if (data.id !== itemId) {
            return data;
        }

        return {
            ...data,
            status: value
        };
    });

    dispatch(addProduct(changeProducts));
};

export const deleteProducts = (data: string) => (dispatch, getState) => {
    const allProducts = getProduct(getState());

    const changeProducts = allProducts.filter(({ id }) => id !== data)

    dispatch(addProduct(changeProducts));
};
