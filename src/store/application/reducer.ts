import { createReducer } from 'redux-act';

import * as applicationActions from './actions';

const initialState = {
    products: []
};

const reducer = createReducer({}, initialState);

reducer.on(applicationActions.addItem, (state, newProduct) => ({
    ...state,
    products: [...state.products, newProduct]
}));

reducer.on(applicationActions.addProduct, (state, newProduct) => ({
    ...state,
    products: [...newProduct]
}));

export default reducer;
