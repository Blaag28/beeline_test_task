import {createSelector} from 'reselect';

import {Product, sortValue} from '@interfaces/index';
import {getSort} from '@store/ui/selectors';
import {sortArr} from '@utils/sort';

import {moduleName} from './actions';

const storeSelector = (store) => store[moduleName];
export const getProduct = createSelector(storeSelector, (store) => store.products);

export const getSortProduct = createSelector(
    getProduct, getSort, (products, {sortByKey, sortMethod}: sortValue) => {

        if (!products) return [];

        products
            .sort((a: Product, b: Product) => sortArr(a[sortByKey], b[sortByKey], sortMethod))
            .sort((a: Product, b: Product) => sortArr(a.status, b.status));

        return {products}
    })



