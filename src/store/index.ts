import { applyMiddleware, compose, createStore } from 'redux';
import { types } from 'redux-act';
import thunkMiddleware from 'redux-thunk';

import combineReducers from './combineReducers';

function getEnhancers() {
    const isBrowserWithExtension = typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;

    if (isBrowserWithExtension) {
        return window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            trace: true,
            traceLimit: 25
        });
    }

    return compose();
}

export function makeStore() {
    /* Добавляем Redux DevTools не на проде */
    const composeEnhancers = getEnhancers();

    /* Библиотека redux-act выдает ошибки при hot reload, выключаем проверку типов не на проде */
    types.disableChecking();

    return createStore(combineReducers, composeEnhancers(applyMiddleware(thunkMiddleware)));
}
