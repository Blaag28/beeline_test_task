import { ModalInputs } from '@interfaces/index';

import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';

import {
    Button,
    FormControl,
    FormLabel,
    Input,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalFooter,
    ModalHeader,
    ModalOverlay,
    Select,
    Stack,
    useDisclosure
} from '@chakra-ui/react';

import { addProducts } from '@store/application/actions';

export const ModalWindow = () => {
    const dispatch = useDispatch();
    const { isOpen, onOpen, onClose } = useDisclosure();

    const { register, handleSubmit } = useForm<ModalInputs>();

    const onSubmit = (data) => {
        dispatch(addProducts(data));
        onClose();
    };

    return (
        <>
            <Button colorScheme='blue' onClick={onOpen}>
                Добавить
            </Button>
            <Modal isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Заполните поля</ModalHeader>
                    <ModalCloseButton />

                    <form onSubmit={handleSubmit(onSubmit)}>
                        <ModalBody pb={6}>
                            <FormControl>
                                <FormLabel>Название товара</FormLabel>
                                <Input name='name' ref={register({ required: true })} placeholder='Название товара' />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Количество</FormLabel>
                                <Input
                                    type='number'
                                    name='count'
                                    ref={register({ required: true })}
                                    placeholder='Количество'
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel>Приоритет</FormLabel>
                                <Select
                                    name='priority'
                                    placeholder='Выберите приоритет'
                                    ref={register({ required: true })}
                                >
                                    <option value='1'>Высокий</option>
                                    <option value='2'>Средний</option>
                                    <option value='3'>Низкий</option>
                                </Select>
                            </FormControl>
                        </ModalBody>
                        <ModalFooter>
                            <Stack direction='row' spacing={4} align='center'>
                                <Button type='submit'>Сохранить</Button>
                                <Button type='reset'>Очистить</Button>
                            </Stack>
                        </ModalFooter>
                    </form>
                </ModalContent>
            </Modal>
        </>
    );
};
