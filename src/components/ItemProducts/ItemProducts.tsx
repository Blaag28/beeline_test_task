import { Product } from '@interfaces/index';

import { useDispatch } from 'react-redux';
import { DeleteIcon } from '@chakra-ui/icons';
import { Badge, Checkbox, Flex, IconButton } from '@chakra-ui/react';

import { changeStatusProducts, deleteProducts } from '@store/application/actions';
import { getLocaleTimes } from '@utils/times';

export const ItemProducts = ({ id, date, name, count, status, priority } : Product) => {
    const dispatch = useDispatch();

    const deleteItem = () => {
        dispatch(deleteProducts(id));
    };

    const changeItem = ({ target }) => {
        dispatch(changeStatusProducts({ itemId: id, value: target.checked }));
    };

    const renderPriority = (priority : string) => {
        switch (true) {
            case priority === '1':
                return (
                    <Badge h='18px' colorScheme='red'>
                        Высокий
                    </Badge>
                );
            case priority === '2':
                return (
                    <Badge h='18px' colorScheme='purple'>
                        Средний
                    </Badge>
                );
            default:
                return <Badge h='18px'>Низкий</Badge>;
        }
    };

    return (
        <Flex key={id} w='70%'>
            <Flex p='2' w='23%'>
                {getLocaleTimes(date)}
            </Flex>
            <Flex p='2' w='14%'>
                {name}
            </Flex>
            <Flex p='2' w='14%'>
                {count}
            </Flex>
            <Flex p='2' w='14%'>
                {renderPriority(priority)}
            </Flex>
            <Flex p='2' w='14%'>
                {status ? (
                    <Badge h='18px' colorScheme='red'>
                        Куплен
                    </Badge>
                ) : (
                    <Badge h='18px' colorScheme='green'>
                        Новый
                    </Badge>
                )}
            </Flex>
            <Flex p='2' w='7%'>
                <Checkbox onChange={changeItem} p='2' m='2' colorScheme='green' defaultIsChecked={status} />
            </Flex>
            <Flex p='2' w='14%'>
                <IconButton
                    p='2'
                    m='2'
                    onClick={deleteItem}
                    colorScheme='blue'
                    aria-label='Удалить'
                    icon={<DeleteIcon />}
                />
            </Flex>
        </Flex>
    );
};
