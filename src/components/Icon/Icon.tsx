import { ArrowDownIcon, ArrowUpIcon } from '@chakra-ui/icons';
import {SORT_METHOD} from "@constants/index";

export const Icon = ({ type }) => {

        if (type === SORT_METHOD.ASC) return <ArrowDownIcon />;

        return <ArrowUpIcon />;
};
