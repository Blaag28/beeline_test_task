import { ModalInputs } from '@interfaces/index';

import { useDispatch, useSelector } from 'react-redux';
import { MinusIcon } from '@chakra-ui/icons';
import { IconButton } from '@chakra-ui/react';

import { Icon } from '@components/Icon';
import { editSort } from '@store/ui/actions';
import {getSort} from "@store/ui/selectors";


export const ButtonSort = ( {name}: ModalInputs) => {
    const dispatch = useDispatch();

    const  { sortMethod, sortByKey } = useSelector(getSort);

    return (
        <IconButton
            size='s'
            onClick={() => dispatch(editSort(name, sortMethod))}
            colorScheme='blue'
            aria-label='Search database'
            icon={name === sortByKey ? <Icon type={sortMethod} /> : <MinusIcon />}
        />
    );
};
