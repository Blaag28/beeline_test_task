import { Product } from '@interfaces/index';

import Head from 'next/head';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Center, Flex } from '@chakra-ui/react';
import { ButtonSort, ItemProducts, ModalWindow } from '@components/index';
import { addProduct } from '@store/application/actions';
import { getSortProduct} from '@store/application/selectors';
import { SORT_BY_KEY } from '@constants/index';
import styles from '../styles/Home.module.css';

export default function Home(): JSX.Element {
    const dispatch = useDispatch();
    const {products} = useSelector(getSortProduct);

    useEffect(() => {
        dispatch(addProduct(JSON.parse(localStorage.getItem('product'))));
    }, []);

    return (
        <Flex className={styles.container} h='100%' w='100%' color='white'>
            <Head>
                <title>Beeline test task</title>
                <link rel='icon' href='/favicon.ico' />
            </Head>
            <Center className={styles.main} w='100%' bg='brand.900' color='white' p={2}>
                <ModalWindow />
                <Flex w='70%'>
                    <Flex p='2' w='23%'>
                        Дата добавления
                        <ButtonSort name={SORT_BY_KEY.DATE}/>
                    </Flex>
                    <Flex p='2' w='14%'>
                        Товар
                    </Flex>
                    <Flex p='2' w='14%'>
                        Количество
                    </Flex>
                    <Flex p='2' w='14%'>
                        Приоритет
                        <ButtonSort name={SORT_BY_KEY.PRIORITY}/>
                    </Flex>
                    <Flex p='2' w='14%'>
                        Статус
                    </Flex>
                    <Flex p='2' w='7%'>
                        Готово
                    </Flex>
                    <Flex p='2' w='14%'>
                        Удалить
                    </Flex>
                </Flex>
                {products.map((products : Product) =>  <ItemProducts key={products.id} {...products} />)}
            </Center>
            <Center className={styles.footer} bg='brand.900' h='100px' color='white'>
                Created by Valery
            </Center>
        </Flex>
    );
}
