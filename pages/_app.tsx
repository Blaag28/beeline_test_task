import { AppProps } from 'next/app';
import { Provider } from 'react-redux';

import { ChakraProvider, extendTheme } from '@chakra-ui/react';
import { getProduct } from '@store/application/selectors';
import { makeStore } from '@store/index';
import { DEFAULT_THEME } from '@constants/index';

import '../styles/globals.css';

const theme = extendTheme(DEFAULT_THEME);

export default function App({ Component, pageProps }: AppProps): JSX.Element {
    const store = makeStore();

    store.subscribe(() => {
        localStorage.setItem('product', JSON.stringify(getProduct(store.getState())));
    });

    return (
        <Provider store={store}>
            <ChakraProvider theme={theme}>
                <Component {...pageProps} />
            </ChakraProvider>
        </Provider>
    );
}
